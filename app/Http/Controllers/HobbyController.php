<?php

namespace App\Http\Controllers;

use App\Hobby;
use Illuminate\Http\Request;
use App\Http\Requests\HobbyRequest;
use Session;
use File;

class HobbyController extends Controller
{
    public function index(){
      $hobbies = Hobby::query()->get();
      return view('hobby.index',compact('hobbies'));
    }
    public function store(Request $request){

     $this->validate($request,[
            'name'=>'required | unique:hobbies',
            'icon' => 'required|mimes:jpeg,png,jpg,gif'
        ]);
      $hobby = new Hobby();
      $hobby->name = $request->name;
      if($request->file('icon')){
            $file= $request->file('icon');
            $filename='images/'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file-> move(public_path('images/'), $filename);
            $hobby->icon= $filename;
        }
        $hobby->save();
        return redirect('hobby')->with('status', 'Data Successfully iserted');
    }
    public function edit($id){
        $data['hobby'] = Hobby::query()->findOrFail($id);
        return view('hobby.edit-hobby')->with($data);
    }
    public function update(Request $request, $id){
        $this->validate($request,[
            'name'=>'required : unique:hobbies',
            'icon' => 'required|mimes:jpeg,png,jpg,gif'
        ]);
        $data = $request->except('icon');
        if($request->hasFile('icon')){
            $file= $request->file('icon');
            $filename='images/'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file-> move(public_path('images/'), $filename);
            $data['icon']= $filename;
        }
        
        $hobby= Hobby::query()->findOrFail($id);
        $hobby->update($data);
        session()->flash('success','hobby Successfully updated.');
        return redirect()->route('hobby.add');
    }
     public function destroy(Hobby $id)
    {
        if ($id) {
            File::delete($id->icon);
            $id->delete();
            Session::flash('status','Hobby Successfully Deleted');
            return redirect()->back();
    
          }
    }
}
