@extends('layouts.Master')

@section('content')
        
        <!-- HOME START-->
        @include('inc.home')
        <!-- HOME END-->
        
        <!-- About Start -->
       @include('inc.about')
        <!-- About end -->
            
        <!-- Services Start -->
        @include('inc.service')
        <!-- Services End -->

        <!-- Resume Start -->
       @include('inc.resume') 
        <!-- Skill End -->

        <!-- Skill & CTA START -->
        @include('inc.skill')
        <!-- Skill & CTA End -->

        <!-- Projects End -->
      @include('inc.artical-resource')
        <!-- Projects End -->

        <!-- Testimonial Start -->
        @include('inc.testimonial')
        <!-- Testimonial End -->

        @include('inc.certifications')

        <!-- Blog Start -->
        @include('inc.artical-resource')
        <!-- Blog Start -->

        <!-- Contact Start -->
        @include('inc.contact')
        <!-- Contact End -->
@endsection