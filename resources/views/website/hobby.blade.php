@extends('website.Master')

@section('content')
<main id="main">

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">
  <div class="container mt-100 mt-60">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="position-relative">
                                <h4 class="title text-uppercase mb-4">Hobbies & Interests</h4>

                                <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                            </div>
                            <p class="text-muted mx-auto para-desc mt-5 mb-0">Obviously I'm a Database Expart. Experienced with all stages of the Database.</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
                
                <div class="row">
                  @forelse (hobbies() as $data)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-4 pt-2">
                        <div class="interests-desc bg-light position-relative px-2 py-3 rounded">
                            <div class="hobbies d-flex align-items-center">
                                <div class="text-center rounded-pill me-4">
                                    <i class="fal fa-coffee"></i>
                                    @if(isset($data->icon))
                                        <img style="display: inline-block; padding: 0px; margin: 0px;" src="{{$data->icon}}" height="30px" width="30px">
                                    @else

                                    @endif
                                </div>
                                <div class="content">
                                    <h6 class="title mb-0">{{$data->name}}</h6>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                  @empty
                  @endforelse
                </div>
            </div><!--end container-->

        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->
@endsection