@extends('layouts.admin')

@section('title','Update Hobby : '.$hobby->name)

@section('content')
<div class="page-titles">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Hobby</a></li>
        <li class="breadcrumb-item active"><a href="javascript:void(0)">Hobby {{ $hobby->name }} Profile</a></li>
    </ol>
</div>

<div class="row">
    {{-- Hobby Form Start --}}
    <div class="col-xl-4 col-lg-4 col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Update Form</h4>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    {{ Form::model($hobby,['route'=>['hobby.update',$hobby->id],'method'=>"post", 'files'=>'true']) }}
                    @include('hobby.hobby-form',["button"=>"Update Category"])
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    {{-- Hobby Form End --}}

    {{-- Hobby Lists Start --}}
    <div class="col-xl-8 col-lg-8 col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Hobby</h4>
            </div>
            <div class="card-body">
                @include('hobby.hobby')
            </div>
        </div>
    </div>
    {{-- Hobby Lists End --}}

</div>

@endsection


@section('js')
<script>
$(document).on("click", ".changeStatus", function() {
    return confirm("Are you sure to change status?");
});
</script>

@endsection