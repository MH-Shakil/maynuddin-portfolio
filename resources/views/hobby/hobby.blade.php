<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Name</th>
                <th>Icon</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>

            @forelse(hobbies() as $key=>$hobby_list)
            <tr>
                <td>{{ $hobby_list->name }}</td>
                <td><img class="rounded-border" height="80px" width="80px" src="{{asset($hobby_list->icon)}}"></td>
                <td>
                    @if(isset($hobby) && $hobby->id == $hobby_list->id)
                    <a href="#" class="badge badge-rounded badge-success">Updating....</a>
                    @else
                    <a href="{{ route('hobby.edit',$hobby_list->id) }}" class="btn-success btn btn-sm"> <i
                            class="fa fa-edit "></i> </a>
                    {{ Form::open(['route'=>['hobby.destroy',$hobby_list->id],"method"=>'post',"class"=>"destroyForm"]) }}
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"> <i class="fa fa-trash"></i> </button>
                    {{ Form::close() }}
                    @endif
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="5">No Hobby Found</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>