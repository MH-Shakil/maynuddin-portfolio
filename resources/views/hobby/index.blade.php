@extends('layouts.admin')

@section('title','New Hobby')

@section('content')
<div class="page-titles">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Hobby</a></li>
        <li class="breadcrumb-item active"><a href="javascript:void(0)">New Hobby</a></li>
    </ol>
</div>

<div class="row">
    {{-- Hobby Form Start --}}
    <div class="col-xl-4 col-lg-4 col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Hobby Form</h4>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    {{ Form::open(['route'=>'hobby.store','method'=>"post", "files"=>"true"]) }}
                    @include('hobby.hobby-form',["button"=>"Save Hobby"])
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    {{-- Hobby Form End --}}

    {{-- Hobby Lists Start --}}
    <div class="col-xl-8 col-lg-8 col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Hobby</h4>
            </div>
            <div class="card-body">
                @include('hobby.hobby')
            </div>
        </div>
    </div>
    {{-- Hobby Lists End --}}

</div>

@endsection


@section('js')
<script>
$(document).on("click", ".changeStatus", function() {
    return confirm("Are you sure to change status?");
});
</script>

@endsection