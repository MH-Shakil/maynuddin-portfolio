<div class="form-group">
    <label>Hobby Name.*</label>
    {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Ex. Sport\'s']) }}
    {{ Form::file('icon',['class'=>'form-control']) }}
</div>

<div class="form-group ">
  {{ Form::submit($button,['class'=>'btn btn-success']) }}
</div>