

        <!-- Navbar Start -->
        <nav id="navbar" class="navbar navbar-expand-lg fixed-top navbar-custom navbar-light sticky">
        <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="images/logo.png" class="l-dark" alt="">
                    <!-- <img src="images/logo-light.png" class="l-light" alt=""> -->
                    <h4 class="l-light text-white">{{user()->name}}</h4>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span data-feather="menu" class="fea icon-md"></span>
                </button><!--end button-->

                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul id="navbar-navlist" class="navbar-nav mx-auto">
                        <li class="nav-item">
                            <a class="nav-link active" href="#home">Home</a>
                        </li><!--end nav item-->
                         <li class="nav-item">
                            <a class="nav-link active" href="#about">About</a>
                        </li><!--end nav item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#news">Artical</a>
                        </li><!--end nav item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#resume">Resume</a>
                        </li><!--end nav item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#projects">Projects</a>
                        </li><!--end nav item-->
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="#news">Blog</a>
                        </li> --><!--end nav item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#hobby">Hobby</a>
                        </li><!--end nav item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#contact">Contact</a>
                        </li><!--end nav item-->
                        
                    </ul>

                    <ul class="list-unstyled mb-0 mt-2 mt-sm-0 social-icon light-social-icon">
                        <li class="list-inline-item"><a href="{{profile()->facebook}}"><i class="mdi mdi-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="{{profile()->twitter}}"><i class="mdi mdi-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="{{profile()->linkedin}}"><i class="mdi mdi-linkedin"></i></a></li>
                    </ul>
                </div> 
            </div><!--end container-->
    </nav><!--end navbar-->
        <!-- Navbar End -->