




        <section class="section pb-0" id="contact">
            <div class="container">
              <div class="container-fluid mt-100 mt-60">
                <div class="rounded-pill py-5" style="background: url('images/hireme.jpg') center center;">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12 text-center">
                                <h4 class="text-light title-dark">I Am Available For Freelancer Projects.</h4>
                                <p class="text-white-50 mx-auto mt-4 para-desc">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                                <div class="mt-4">
                                    <a href="#contact" class="btn btn-primary mouse-down">Hire me <i data-feather="chevron-down" class="fea icon-sm"></i></a>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end container-->
                </div><!--end div-->
            </div><!--end container fluid-->
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="position-relative">
                                <h4 class="title text-uppercase mb-4">Contact Me</h4>
                                <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div class="row">
                    <div class="col-md-4 mt-4 pt-2">
                        <div class="contact-detail text-center">
                            <div class="icon">
                                <i data-feather="phone" class="fea icon-md"></i>
                            </div>
                            <div class="content mt-4">
                                <h5 class="title text-uppercase">Phone</h5>
                                <p class="text-muted">Promising development turmoil inclusive education transformative community</p>
                                <a href="tel:+152534-468-854" class="text-primary">{{ profile()->phone }}</a>
                            </div>  
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-4 mt-4 pt-2">
                        <div class="contact-detail text-center">
                            <div class="icon">
                                <i data-feather="mail" class="fea icon-md"></i>
                            </div>
                            <div class="content mt-4">
                                <h5 class="title text-uppercase">Email</h5>
                                <p class="text-muted">Promising development turmoil inclusive education transformative community</p>
                                <a href="mailto:contact@example.com" class="text-primary">{{ profile()->email }}</a>
                            </div>  
                        </div>
                    </div><!--end col-->
                    
                    <div class="col-md-4 mt-4 pt-2">
                        <div class="contact-detail text-center">
                            <div class="icon">
                                <i data-feather="map-pin" class="fea icon-md"></i>
                            </div>
                            <div class="content mt-4">
                                <h5 class="title text-uppercase">Location</h5>
                                <p class="text-muted">{{ profile()->current_address }}</p>
                                <a href="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d6030.418742494061!2d-111.34563870463673!3d26.01036670629853!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1471908546569" class="video-play-icon text-primary">View on Google map</a>
                            </div>  
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->

        <section class="section pt-5 mt-3">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="custom-form mb-sm-30">
                            <form method="post" name="myForm" onsubmit="return validateForm()">
                                <p id="error-msg"></p>
                                <div id="simple-msg"></div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-6">
                                                <div class="form-group">
                                                    <input name="name" id="name" type="text" class="form-control border rounded" placeholder="First Name :">
                                                </div>
                                            </div><!--end col-->
                                            <div class="col-lg-12 col-md-6">
                                                <div class="form-group">
                                                    <input name="email" id="email" type="email" class="form-control border rounded" placeholder="Your email :">
                                                </div> 
                                            </div><!--end col-->
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <input name="subject" id="subject" class="form-control border rounded" placeholder="Your subject :">
                                                </div>                                                                               
                                            </div><!--end col-->
                                        </div><!--end row-->
                                    </div><!--end col-->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <textarea name="comments" id="comments" rows="4" class="form-control border rounded" placeholder="Your Message :"></textarea>
                                        </div>
                                    </div><!--end col-->

                                    <div class="col-sm-12 text-end">
                                        <button type="submit" id="submit" name="send" class="btn btn-primary">Send Message</button>
                                    </div>
                                </div>
                            </form>
                        </div><!--end custom-form-->
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->