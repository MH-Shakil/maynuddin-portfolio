     <section class="section pb-0" id="contact">
            <div class="container">
            <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="position-relative">
                                <h4 class="title text-uppercase mb-4">Certification</h4>
                                <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

            <div class="row skills-content">
                @forelse (graduations() as $graduation)
                <div class="col-lg-6">
                    <h2 class="text-center">{{ $graduation->certificate->name }}</h2>
                    <ul class="list-group">
                        <li class="list-group-item">Title: <strong class="text-right" style="float: right;"> {{ $graduation->title }} </strong></li>
                        <li class="list-group-item">Institute: <strong class="text-right" style="float: right;"> {{ $graduation->institute }} </strong></li>
                        <li class="list-group-item">Session: <strong class="text-right" style="float: right;"> {{ $graduation->session }} </strong></li>
                        <li class="list-group-item">Passing Year: <strong class="text-right" style="float: right;"> {{ $graduation->passing_year }} </strong></li>
                        <li class="list-group-item">Comments: <strong class="text-right" style="float: right;"> {{ $graduation->notes }} </strong></li>
                    </ul>
                </div>
                @empty

                @endforelse
            </div>
            </div><!--end container-->
        </section><!--end section-->