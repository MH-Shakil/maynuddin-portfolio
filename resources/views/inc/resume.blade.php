 <section class="section" id="resume">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="position-relative">
                                <h4 class="title text-uppercase mb-4">Work Participation</h4>
                                <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                            </div>
                            <p class="text-muted mx-auto para-desc mt-5 mb-0">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div class="row">
                    <div class="col-12">
                        <div class="main-icon rounded-pill text-center mt-4 pt-2">
                            <i data-feather="star" class="fea icon-md-sm"></i>
                        </div>
                        <div class="timeline-page pt-2 position-relative">

                           @forelse (experiences() as $experience)
                            @if($experience->id % 2 ==1)
                            <div class="timeline-item mt-4">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="duration date-label-left border rounded p-2 ps-4 pe-4 position-relative shadow text-start"> {{ $experience->join_date }} to {{ $experience->left_date }} </div>
                                    </div><!--end col-->
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="event event-description-right rounded p-4 border float-left text-start">
                                            <h5 class="title mb-0 text-capitalize">{{ $experience->designation }}</h5>
                                            <small class="company">{{ $experience->organization }}</small>
                                            <p class="timeline-subtitle mt-3 mb-0 text-muted">{{ $experience->body }}</p>
                                        </div>
                                    </div><!--end col-->
                                </div><!--end row-->
                            </div><!--end timeline item-->
                            @else
                              <div class="timeline-item mt-4">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 order-sm-1 order-2">
                                        <div class="event event-description-left rounded p-4 border float-left text-end">
                                            <h5 class="title mb-0 text-capitalize">{{ $experience->designation }}</h5>
                                            <small class="company">{{ $experience->organization }}</small>
                                            <p class="timeline-subtitle mt-3 mb-0 text-muted">{{ $experience->body }}</p>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-lg-6 col-md-6 col-sm-6 order-sm-2 order-1">
                                        <div class="duration duration-right rounded border p-2 ps-4 pe-4 position-relative shadow text-start"> {{ $experience->join_date }} to {{ $experience->left_date }} </div>
                                    </div><!--end col-->
                                </div><!--end row-->
                            </div><!--end timeline item-->
                            @endif
                           @empty

                          @endforelse
                        </div><!--end timeline page-->
                        <!-- TIMELINE END -->
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->  