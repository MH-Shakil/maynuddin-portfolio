  <footer class="footer bg-dark">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <a href="#"><img src="images/logo-light.png" alt=""></a>
                        <p class="para-desc mx-auto mt-5">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                        <ul class="list-unstyled mb-0 mt-4 social-icon">
                            <li class="list-inline-item"><a href="{{profile()->facebook}}"><i class="mdi mdi-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="{{profile()->twitter}}"><i class="mdi mdi-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="{{profile()->facebook}}"><i class="mdi mdi-instagram"></i></a></li>
                            <li class="list-inline-item"><a href="{{profile()->website}}"><i class="mdi mdi-vimeo"></i></a></li>
                            <li class="list-inline-item"><a href="{{profile()->google}}"><i class="mdi mdi-google-plus"></i></a></li>
                            <li class="list-inline-item"><a href="{{profile()->linkedin}}"><i class="mdi mdi-linkedin"></i></a></li>
                        </ul>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </footer><!--end footer-->
        <footer class="footer footer-bar bg-dark">
            <div class="container text-foot text-center">
                <p class="mb-0">© <script>document.write(new Date().getFullYear())</script> {{user()->name}}. Design with <i class="mdi mdi-heart text-danger"></i> by <a href="http://shreethemes.in/" target="_blank" class="text-reset"> {{user()->name}}</a>.</p>
            </div><!--end container-->
        </footer><!--end footer-->