  <section class="section bg-light" id="projects">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="position-relative">
                                <h4 class="title text-uppercase mb-4">My Portfolio</h4>
                                <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-4">
                    <img style="max-width: 100%; height: auto; height: 321px; width: 400px;" src="{{ asset('project_files/picture/'.user()->avatar) }}" class="img-fluid" alt="">
                </div>
                <div class="col-lg-8 pt-4 pt-lg-0 content">
                    <h3>Database Expert</h3>
                    <p class="fst-italic">

                    </p>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul>
                                <li><i class="bi bi-rounded-right"></i>
                                    <strong>Birthday:</strong>{{ \Carbon\Carbon::parse(profile()->dob)->format('d M Y')}}
                                </li>
                                <li><i class="bi bi-rounded-right"></i> <strong>Website:</strong>
                                    {{ profile()->website }}
                                </li>
                                <li><i class="bi bi-rounded-right"></i> <strong>Phone:</strong>
                                    {{ profile()->website }}
                                </li>
                                <li><i class="bi bi-rounded-right"></i> <strong>City:</strong>Chattogram</li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul>
                                <li><i class="bi bi-rounded-right"></i> <strong>Degree:</strong>
                                    {{ profile()->degree }}
                                </li>
                                <li><i class="bi bi-rounded-right"></i> <strong>Email:</strong>
                                    {{ profile()->email }}
                                </li>
                                <li><i class="bi bi-rounded-right"></i> <strong>Freelance:</strong> Available</li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div><!--end row-->
            </div>    
            
            <div class="container">
                <div class="row mt-4">
                    <ul class="col portfolioFilter text-center mb-0" id="filter">
                        <li class="list-inline-item mt-2"><a href="javascript:void(0)" class="categories tab-active shadow active text-dark me-2 py-2 px-3 rounded" onclick="filterSelection('all')">All</a></li>

                        @forelse (categories() as $category)
                        <li class="list-inline-item mt-2"><a href="javascript:void(0)" class="categories tab-active shadow text-dark me-2 py-2 px-3 rounded" onclick="filterSelection('branding')">{{$category->name}}</a></li>
                         @empty

                         @endforelse

                    </ul>
                </div>
                <!-- Gallary -->
                <div class="row">
                       @forelse (myArticles() as $key=>$article)
                    <div class="col-lg-4 col-md-6 mt-4 pt-2 filter-box branding designing">
                        <div class="item-box portfolio-box rounded shadow bg-white overflow-hidden">
                            <div class="portfolio-box-img position-relative overflow-hidden">
                                <img class="item-container img-fluid mx-auto" src="{{  $article->files->count()>0 ? asset('project_files/articleResource/'.$article->files[0]->name) : asset('website/assets/img/portfolio/portfolio-3.jpg') }}" alt="1" />
                                <div class="overlay-work">
                                    <div class="work-content text-center">
                                        <a href="javascript:void(0)" data-src="images/portfolio/1.jpg" data-gallery="myGal" class="text-light work-icon bg-dark d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm image-icon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="gallary-title py-4 text-center">
                                <h5><a href="{{  route('detailsPage',$article->id)}}" class="title text-dark">{{ $article->title}}</a></h5>
                                <span>{{ $article->category->name }}</span>
                            </div>
                        </div>
                    </div>
                    @empty

                    @endforelse

                    <!-- <div class="col-lg-4 col-md-6 mt-4 pt-2 filter-box branding development">
                        <div class="item-box portfolio-box rounded shadow bg-white overflow-hidden">
                            <div class="portfolio-box-img position-relative overflow-hidden">
                                <img class="item-container img-fluid mx-auto" src="images/portfolio/2.jpg" alt="1" />
                                <div class="overlay-work">
                                    <div class="work-content text-center">
                                        <a href="javascript:void(0)" data-src="images/portfolio/2.jpg" data-gallery="myGal" class="text-light work-icon bg-dark d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm image-icon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="gallary-title py-4 text-center">
                                <h5><a href="page-portfolio-detail.html" class="title text-dark">The Micro Headphones</a></h5>
                                <span>Development</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6 mt-4 pt-2 filter-box designing development">
                        <div class="item-box portfolio-box rounded shadow bg-white overflow-hidden">
                            <div class="portfolio-box-img position-relative overflow-hidden">
                                <img class="item-container img-fluid mx-auto" src="images/portfolio/3.jpg" alt="1" />
                                <div class="overlay-work">
                                    <div class="work-content text-center">
                                        <a href="javascript:void(0)" data-src="images/portfolio/3.jpg" data-gallery="myGal" class="text-light work-icon bg-dark d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm image-icon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="gallary-title py-4 text-center">
                                <h5><a href="page-portfolio-detail.html" class="title text-dark">The Coffee Cup</a></h5>
                                <span>Designing</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6 mt-4 pt-2 filter-box photography">
                        <div class="item-box portfolio-box rounded shadow bg-white overflow-hidden">
                            <div class="portfolio-box-img position-relative overflow-hidden">
                                <img class="item-container img-fluid mx-auto" src="images/portfolio/4.jpg" alt="1" />
                                <div class="overlay-work">
                                    <div class="work-content text-center">
                                        <a href="javascript:void(0)" data-src="images/portfolio/4.jpg" data-gallery="myGal" class="text-light work-icon bg-dark d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm image-icon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="gallary-title py-4 text-center">
                                <h5><a href="page-portfolio-detail.html" class="title text-dark">The Wooden Desk</a></h5>
                                <span>Photography</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6 mt-4 pt-2 filter-box photography">
                        <div class="item-box portfolio-box rounded shadow bg-white overflow-hidden">
                            <div class="portfolio-box-img position-relative overflow-hidden">
                                <img class="item-container img-fluid mx-auto" src="images/portfolio/5.jpg" alt="1" />
                                <div class="overlay-work">
                                    <div class="work-content text-center">
                                        <a href="javascript:void(0)" data-src="images/portfolio/5.jpg" data-gallery="myGal" class="text-light work-icon bg-dark d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm image-icon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="gallary-title py-4 text-center">
                                <h5><a href="page-portfolio-detail.html" class="title text-dark">Camera</a></h5>
                                <span>Illustrations</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6 mt-4 pt-2 filter-box photography">
                        <div class="item-box portfolio-box rounded shadow bg-white overflow-hidden">
                            <div class="portfolio-box-img position-relative overflow-hidden">
                                <img class="item-container img-fluid mx-auto" src="images/portfolio/6.jpg" alt="1" />
                                <div class="overlay-work">
                                    <div class="work-content text-center">
                                        <a href="javascript:void(0)" data-src="images/portfolio/6.jpg" data-gallery="myGal" class="text-light work-icon bg-dark d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm image-icon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="gallary-title py-4 text-center">
                                <h5><a href="page-portfolio-detail.html" class="title text-dark">Branded Laptop</a></h5>
                                <span>Photoshop</span>
                            </div>
                        </div>
                    </div> -->
                </div>

                <div class="row">
                    <div class="col-lg-12 mt-4 pt-2">
                        <div class="text-center">
                            <a href="page-portfolio.html" class="btn btn-outline-primary">More works <i data-feather="refresh-cw" class="fea icon-sm"></i></a>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->