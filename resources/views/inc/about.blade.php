  <section class="section pb-3" id="about">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-5">
                        <div class="about-hero">
                            <img src="{{ asset('project_files/picture/'.user()->avatar) }}" class="img-fluid mx-auto d-block about-tween position-relative" alt="">
                        </div>
                    </div><!--end col-->

                    <div class="col-lg-7 col-md-7 mt-4 pt-2 mt-sm-0 pt-sm-0">
                        <div class="section-title mb-0 ms-lg-5 ms-md-3">
                            <h4 class="title text-primary mb-3">{{ user()->name }}</h4>
                            <h6 class="designation mb-3">I'm a Passionate <span class="text-primary">Web Design</span></h6>
                            <p class="text-muted">{{ profile()->about}}</p>
                            <div class="mt-4">
                                <a href="#projects" class="btn btn-primary mouse-down">View Portfolio</a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->

            <div class="container mt-100 mt-60" id="hobby">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="position-relative">
                                <h4 class="title text-uppercase mb-4">Hobbies & Interests</h4>

                                <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
                
                <div class="row">
                  @foreach( hobbies() as $data)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-4 pt-2">
                        <div class="interests-desc bg-light position-relative px-2 py-3 rounded">
                            <div class="hobbies d-flex align-items-center">
                                <div class="text-center rounded-pill me-4">
                                    <img height="30px" width="30px" src="{{$data->icon}}">
                                </div>
                                <div class="content">
                                    <h6 class="title mb-0">{{$data->name}}</h6>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                    @endforeach
                </div>
            </div><!--end container-->

        </section>