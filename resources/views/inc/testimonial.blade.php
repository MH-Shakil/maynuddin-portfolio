  <section class="section pb-3" id="about">
            <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="position-relative">
                                <h4 class="title text-uppercase mb-4">Testimonials</h4>
                                <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                            </div>
                            <p class="text-muted mx-auto para-desc mt-5 mb-0">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-5">
                        <div class="about-hero">
                            <img src="{{ asset('website/assets') }}/img/testimonials/testimonials-1.jpg" class="img-fluid mx-auto d-block about-tween position-relative" alt="">
                        </div>
                    </div><!--end col-->

                    <div class="col-lg-7 col-md-7 mt-4 pt-2 mt-sm-0 pt-sm-0">
                        <div class="section-title mb-0 ms-lg-5 ms-md-3">
                            <h4 class="title text-primary mb-3">{{ user()->name }}</h4>
                           <h6><span class="text-primary">Ceo &amp; Founder</span></h6>
                            <p class="text-muted">{{ profile()->about}}</p>
                            <img src="images/sign.png" height="22" alt="">
                            <div class="mt-4">
                                <a href="#projects" class="btn btn-primary mouse-down">View Portfolio</a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->


    </section>