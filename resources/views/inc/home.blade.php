
        /*<section class="bg-home d-table w-100" style="background-image:url('{{ asset('project_files/picture/\'.profile()->slider_bg_img) }}')" id="home">*/
            <div class="bg-overlay"></div>
            <div class="container position-relative" style="z-index: 1;">
                <div class="row mt-5 mt-sm-0 justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="title-heading">
                            <img src="{{ asset('project_files/picture/'.user()->avatar) }}" class="img-fluid rounded-circle" alt="">
                            <h3 class="heading text-light title-dark mt-3">I Am <span class="typewrite text-primary" data-period="2000" data-type="[ &quot;Web Designer&quot;, &quot;Web Developer&quot;, &quot;Photographer&quot;]"><span class="wrap">{{ user()->name }}</span></span></h3>
                            <p class="para-desc mx-auto text-white-50">{{profile()->about}}</p>
                            <div class="mt-4">
                                <a href="{{ asset('project_files/resume/'.user()->resume) }}" class="btn btn-primary rounded">Download CV <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download fea icon-sm"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg></a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container--> 
            <a href="#about" data-scroll-nav="1" class="mouse-icon mouse-icon-white rounded-pill bg-transparent mouse-down">
                <span class="wheel position-relative d-block mover"></span>
            </a>
        <canvas class="particles-js-canvas-el" width="1349" height="915" style="width: 100%; height: 100%; position: absolute; top: 0px;"></canvas></section>


        