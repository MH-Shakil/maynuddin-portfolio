 <section class="section bg-light pb-3" id="news">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="position-relative">
                                <h4 class="title text-uppercase mb-4">Artical & Resource</h4>
                                <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

               <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

                @forelse (myArticles() as $key=>$article)

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                        <div class="blog-post rounded shadow">
                            <img src="{{  $article->files->count()>0 ? asset('project_files/articleResource/'.$article->files[0]->name) : asset('website/assets/img/portfolio/portfolio-3.jpg') }}" class="img-fluid rounded-top" alt="">
                            <div class="content pt-4 pb-4 p-3">
                                <ul class="list-unstyled d-flex justify-content-between post-meta">
                                    <li><i data-feather="user" class="fea icon-sm me-1"></i><a href="javascript:void(0)" class="text-dark"> {{ $article->author }}</a></li> 
                                    <li><i data-feather="tag" class="fea icon-sm me-1"></i><a href="javascript:void(0)" class="text-dark">Category: {{ $article->category->name }}</a></li>                                    
                                </ul>  
                                <h5 class="mb-3"><a href="page-blog-detail.html" class="title text-dark">{{ $article->title}}</a></h5> 
                                <ul class="list-unstyled mb-0 pt-3 border-top d-flex justify-content-between">
                                    <li><a href="{{  route('detailsPage',$article->id)}}" class="text-dark">Read More <i data-feather="chevron-right" class="fea icon-sm"></i></a></li>
                                    <li><i class="mdi mdi-calendar-edit me-1"></i>{{ $article->year }}</li>
                                </ul>
                            </div><!--end content-->
                        </div><!--end blog post-->
                    </div><!--end col-->
                @empty

                @endforelse


            </div>
            </div><!--end container-->
        </section><!--end section-->