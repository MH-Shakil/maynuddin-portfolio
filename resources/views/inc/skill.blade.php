 <section class="cta-full border-top">
            <div class="container-fluid">
                <div class="row position-relative">
                    <div class="col-lg-4 padding-less img" style="background-image:url('images/skills.jpg')" data-jarallax='{"speed": 0.5}'></div><!-- end col -->
                    <div class="col-lg-8 offset-lg-4">
                        <div class="cta-full-img-box">
                            <div class="row justify-content-center">
                                <div class="col-12 text-center">
                                    <div class="section-title">
                                        <div class="position-relative">
                                            <h4 class="title text-uppercase mb-4">Work Expertise</h4>
                                            <div>
                                    <div class="title-box"></div>
                                    <div class="title-line"></div>
                                </div>
                                        </div>
                                        <p class="text-muted mx-auto para-desc mt-5 mb-0">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                                    </div>
                                </div><!--end col-->
                            </div><!--end row-->

                            <div class="row align-items-center">
                                <div class="col-lg-9 col-md-8 col-12">
                                   @forelse (skills() as $skill)
                                    <div class="tab-content ps-lg-4" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-cloud" role="tabpanel" aria-labelledby="pills-cloud-tab">
                                            <div class="progress-box mt-4 pt-2">
                                                <h6 class="font-weight-normal">{{ $skill->name}}</h6>
                                                <div class="progress">

                                                    <div class="progress-bar position-relative bg-primary" style="width:<?php echo $skill->percentage.'%'; ?>">
                                                        <div class="progress-value d-block text-dark h6">{{ $skill->percentage}}</div>
                                                    </div>
                                                </div>
                                        </div><!--end teb pane-->
                                    </div><!--end tab content-->
                                     </div><!--end col-->
                                    @empty

                                  @endforelse
                               
                            </div> <!-- end row --> 
                        </div> <!-- end about detail -->
                    </div> <!-- end col -->        
                </div><!--end row-->
            </div><!--end container fluid-->
        </section><!--end section--> 