
<!DOCTYPE html>
    <html lang="en">

    
<!-- Mirrored from shreethemes.in/cristino/layouts/index-particles.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 May 2022 20:19:51 GMT -->
<head>
        <meta charset="utf-8" />
        <title>{{user()->name}} - Personal Portfolio</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 5 Landing Page Template" />
        <meta name="keywords" content="Bootstrap 5, premium, marketing, multipurpose" />
        <meta content="Shreethemes" name="author" />
        <!-- favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico')}}">
        <!-- Bootstrap -->
        <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- MK Lightbox -->
        <link href="{{ asset('css/mklb.css')}}" rel="stylesheet" type="text/css" />
        <!-- Icon -->
        <link href="{{ asset('css/materialdesignicons.min.css')}}" rel="stylesheet" type="text/css" />
                  
        <!-- SLIDER -->
        <link rel="stylesheet" href="{{ asset('css/tiny-slider.css')}}"/>    
        <!-- Custom Css -->
        <link href="{{ asset('css/style.min.css')}}" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="{{ asset('css/colors/light-green.css')}}" rel="stylesheet" id="color-opt">

    </head>

    <body data-bs-spy="scroll" data-bs-target="#navbar-navlist" data-bs-offset="80">
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="logo">
                    <h4 class="text-center">{{user()->name}}</h4>
                </div>
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->
        @include('inc.canva')
        <!-- Navbar Start -->
        @include('inc.top-nav')

         @yield('content')

        <!-- Footer Start -->
        @include('inc.footer')
        <!-- Footer End -->
        <!-- Back to top -->
        <a href="#" onclick="topFunction()" class="back-to-top rounded text-center" id="back-to-top"> 
            <i class="mdi mdi-chevron-up d-block"> </i> 
        </a>
        <!-- Back to top -->

        <!-- Style switcher -->
        <div id="style-switcher" class="bg-light border p-3 pt-2 pb-2" onclick="toggleSwitcher()">
            <div>
                <h6 class="title text-center">Select Your Color</h6>
                <ul class="pattern">
                    <li class="list-inline-item">
                        <a class="color1" href="javascript: void(0);" onclick="setColor('default')"></a>
                    </li>
                    <li class="list-inline-item">
                        <a class="color2" href="javascript: void(0);" onclick="setColor('blue')"></a>
                    </li>
                    <li class="list-inline-item">
                        <a class="color3" href="javascript: void(0);" onclick="setColor('light-green')"></a>
                    </li>
                    <li class="list-inline-item">
                        <a class="color4" href="javascript: void(0);" onclick="setColor('yellow')"></a>
                    </li>
                    <li class="list-inline-item">
                        <a class="color5" href="javascript: void(0);" onclick="setColor('light-yellow')"></a>
                    </li>
                    <li class="list-inline-item">
                        <a class="color6" href="javascript: void(0);" onclick="setColor('purple')"></a>
                    </li>
                </ul>

                <h6 class="title text-center pt-3 mb-0 border-top">Theme Option</h6>
                <ul class="text-center list-unstyled mb-0">
                    <li class="d-grid"><a href="javascript:void(0)" class="btn btn-sm btn-block btn-primary rtl-version t-rtl-light mt-2" onclick="setTheme('style-rtl')">RTL</a></li>
                    <li class="d-grid"><a href="javascript:void(0)" class="btn btn-sm btn-block btn-primary ltr-version t-ltr-light mt-2" onclick="setTheme('style')">LTR</a></li>
                    <li class="d-grid"><a href="javascript:void(0)" class="btn btn-sm btn-block btn-primary dark-rtl-version t-rtl-dark mt-2" onclick="setTheme('style-dark-rtl')">RTL</a></li>
                    <li class="d-grid"><a href="javascript:void(0)" class="btn btn-sm btn-block btn-primary dark-ltr-version t-ltr-dark mt-2" onclick="setTheme('style-dark')">LTR</a></li>
                    <li class="d-grid"><a href="javascript:void(0)" class="btn btn-sm btn-block btn-dark dark-version t-dark mt-2" onclick="setTheme('style-dark')">Dark</a></li>
                    <li class="d-grid"><a href="javascript:void(0)" class="btn btn-sm btn-block btn-dark light-version t-light mt-2" onclick="setTheme('style')">Light</a></li>
                </ul>

                <h6 class="title text-center pt-3 mb-0 border-top">Download</h6>
                <ul class="text-center list-unstyled mb-0">
                    <li><a href="https://1.envato.market/cristino" target="_blank" class="btn btn-sm btn-block btn-warning mt-2 w-100">Download</a></li>
                </ul>
            </div>
            <div class="bottom p-0">
                <a href="javascript: void(0);" class="settings bg-white shadow d-block"><i class="mdi mdi-settings ms-1 mdi-24px position-absolute mdi-spin text-primary"></i></a>
            </div>
        </div>
        <!-- end Style switcher -->

        <!-- javascript -->
        <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
        <!-- SLIDER -->
        <script src="{{asset('js/tiny-slider.js')}}"></script>
        <script src="{{asset('js/tiny-slider-init.js')}}"></script>
        <!-- MK Lightbox -->
        <script src="{{asset('js/mklb.js')}}"></script>
        <script src="{{asset('js/filter.init.js')}}"></script>
        <!-- Contact -->
        <script src="{{asset('js/contact.js')}}"></script>
        <!-- Counter -->
        <script src="{{asset('js/counter.init.js')}}"></script>
        <!-- Feather icon -->
        <script src="{{asset('js/feather.min.js')}}"></script>
        <!-- Switcher -->
        <script src="{{asset('js/switcher.js')}}"></script>
        <!-- Main Js -->
        <script src="{{asset('js/app.js')}}"></script>
    </body>

<!-- Mirrored from shreethemes.in/cristino/layouts/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 May 2022 20:23:48 GMT -->
</html>